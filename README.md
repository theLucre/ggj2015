# HAND IN PROCEDURE #

Details on the format of the final GGJ deliverable found here:
[http://archive.globalgamejam.org/wiki/hand-procedure](http://archive.globalgamejam.org/wiki/hand-procedure)

* /src/ => the full sourcecode with all assets of the project
* /release/ the distributable files including a README.TXT with full installation instructions
* /press/ one hi-res image called press.jpg to be used for GGJ PR (1024x768 or better)
* /other/ additional media, photos, videos
* license.txt This is a small text file with precisely the content described here http://www.globalgamejam.org/content/license-and-distribution-agreement (just copy-paste the complete contents of the section License File Contents into license.txt)

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact