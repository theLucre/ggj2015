﻿using UnityEngine;
using System.Collections;

public class ThoughtAreaScript : MonoBehaviour {

	public ThoughtsScript ThoughtManager;
	public Sprite sprite;

	void OnTriggerEnter2D(Collider2D other) {
		if( other.gameObject.tag == "Enemy") {
			ThoughtManager.SetThought( sprite );
		}
	}
}
