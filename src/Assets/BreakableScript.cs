﻿using UnityEngine;
using System.Collections;

public class BreakableScript : MonoBehaviour {

	public string SoundFX;
	public Sprite Broken;
	SpriteRenderer sr;
	bool IsBroken = false;

	// Use this for initialization
	void Start () {
		sr = gameObject.GetComponent<SpriteRenderer>();
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "Enemy" && !IsBroken) {
			IsBroken = true;
			sr.sprite = Broken;
			collider2D.enabled = false;
			Sound.Instance.PlaySFX( SoundFX );
		}
	}
}
