﻿using UnityEngine;
using System.Collections;

public class BGMScript : MonoBehaviour {

	private static BGMScript instance = null;
	public static BGMScript Instance {
		get { return instance; }
	}

	void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}
}
