﻿using UnityEngine;
using System.Collections;

public class FaceScript : MonoBehaviour {

	public float Range = 0.25f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale = Vector3.one + (Vector3.one * Mathf.Sin ( Time.frameCount * 0.04f) * Range);
	}
}
