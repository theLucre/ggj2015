﻿using UnityEngine;
using System.Collections;

public class ThoughtsScript : MonoBehaviour {

	public Transform Gramps;
	SpriteRenderer sr;
	float ThoughtTime = 2.5f;

	void Start() {
		sr = gameObject.GetComponent<SpriteRenderer>();
		sr.enabled = false;
	}

	// Update is called once per frame
	void Update () {
		Vector3 pos = Gramps.position;
		pos.y += 1.8f;
		pos.x += 0.2f;
		pos.z = transform.position.z;
		transform.position = pos;

		ThoughtTime -= Time.deltaTime;
		if( ThoughtTime <= 0 )
			sr.enabled = false;
	}

	public void SetThought(Sprite sprite) {
		sr.sprite = sprite;
		sr.enabled = true;
		ThoughtTime = 3;
	}
}
