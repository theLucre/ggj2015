﻿using UnityEngine;
using System.Collections;

public class StartButtonScript : MonoBehaviour {

	bool IsDown = false;
	public GameObject Menu;

	// Use this for initialization
	void Start () {
		Menu.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag =="Cursor") {
			Toggle();
		}
	}

	void Toggle() {
		Sound.Instance.PlaySFX(Sound.CLICK);
		if(IsDown) {
			IsDown = false;
			Menu.SetActive(false);
		} else {
			IsDown = true;
			Menu.SetActive(true);
		}

		           
	}
}
