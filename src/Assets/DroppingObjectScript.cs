﻿using UnityEngine;
using System.Collections;

public class DroppingObjectScript : MonoBehaviour {
	float Angle;
	float Speed;
	// Use this for initialization
	void Start () {
		Randomize();
	}
	
	// Update is called once per frame
	void Update () {
		Angle += Time.deltaTime * Speed;
		transform.rotation = Quaternion.Euler (0, 0, Angle);
		Vector3 pos = transform.position;
		pos.y +=  -Speed * Time.deltaTime * 0.1f;
		transform.position = pos;
	}

	void OnBecameInvisible() {
		Randomize();
	}

	void Randomize() {
		Speed = Random.Range (20, 70);
		Vector3 pos = transform.position;
		pos.y = 5.79f;
		pos.x = Random.Range (-5f,5f);
		transform.position = pos;
	}
}
