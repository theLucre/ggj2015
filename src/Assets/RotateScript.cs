﻿using UnityEngine;
using System.Collections;

public class RotateScript : MonoBehaviour {

	public float Speed;
	public float Angle = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Angle += Time.deltaTime * Speed;
		transform.rotation = Quaternion.Euler (0, 0, Angle);
	}


}
