﻿using UnityEngine;
using System.Collections;

public class CreditsScript : MonoBehaviour {
	public float Speed= 10;
	public float Delay = 2f;

	// Use this for initialization
	void Start () {
		//KIlL INTRO SONG
		Destroy (GameObject.FindObjectOfType<BGMScript>().gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		Delay -= Time.deltaTime;

		if(Delay < 0)
			transform.Translate(0,Speed * Time.deltaTime, 0);

		if( transform.position.y > 35) 
			Application.LoadLevel("logo");
	}
}
