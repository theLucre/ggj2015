﻿using UnityEngine;
using System.Collections;

public class KeyScript : MonoBehaviour {

	public Color HighlightColor;
	public Sprite OnFrame, OffFrame;
	public string Character;
	SpriteRenderer sr;
	public PlayerComputerScript Cursor;
	bool IsHighlighted = false;

	// Use this for initialization
	void Start () {
		sr= gameObject.GetComponent<SpriteRenderer>();
		Invoke ("RandomHighlight", Random.Range(0f,10f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D( Collider2D other ) {
		if( other.gameObject.tag == "Cursor" && Cursor.Pressing) {
			Click ();
			CancelInvoke( "Click");
			if( IsHighlighted ) {
				UnHighlight();
				Cursor.AddKey(  );
			}
			Cursor.AddTotalKey(Character);
		}
	}

	void OnTriggerExit2D( Collider2D other ) {
		if( other.gameObject.tag == "Cursor" )
			Invoke( "UnClick", 0.1f);
	}

	void Click() {
		sr.sprite = OnFrame;
		Sound.Instance.PlaySFX(Sound.KEY);
	}

	void UnClick() {
		sr.sprite = OffFrame;
	}

	void RandomHighlight() {
		sr.color = HighlightColor;
		IsHighlighted = true;
		Invoke ("UnHighlight", 3f);
		Invoke ("RandomHighlight", Random.Range(5f, 15f));
	}

	void UnHighlight() {
		IsHighlighted = false;
		sr.color = Color.white;
	}

}
