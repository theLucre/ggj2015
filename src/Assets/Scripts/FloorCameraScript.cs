﻿using UnityEngine;
using System.Collections;

public class FloorCameraScript : MonoBehaviour {

	public Transform Target;
	public float Speed;
	public float dampTime = 0.5f;
	private Vector3 velocity = Vector3.zero;
	public Collider2D CamBounds;
	public float Delay;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
//		Vector3 dir = Target.position - transform.position;
//		dir.Normalize();
//		rigidbody2D.AddForce( dir * Time.deltaTime * Speed );

		Delay -= Time.deltaTime;

		if(Delay > 0 ) return;

		// smoothFollow 
		if (Target )
		{
			Vector3 point = camera.WorldToViewportPoint(Target.transform.position);
			Vector3 delta = Target.transform.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			Vector3 destination = transform.position + delta;
			Vector3 pos = Vector3.SmoothDamp(transform.position, Target.position, ref velocity, dampTime);
			pos.z = -10;

			// cap bounds 
			transform.position = pos;
		}
	}
}
