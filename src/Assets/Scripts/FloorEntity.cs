﻿using UnityEngine;
using System.Collections;

public class FloorEntity : MonoBehaviour {

	public float Speed;
	public float Damping = 0.8f;
	public Animator anim;

	public virtual void Start() {
		anim = gameObject.GetComponent<Animator>();
	}

	protected void MoveRight ()
	{
		rigidbody2D.AddForce( Vector2.right * Time.deltaTime * Speed );
		transform.rotation = Quaternion.Euler (0, 0, 225);

	}
	
	protected void MoveLeft ()
	{
		rigidbody2D.AddForce( -Vector2.right * Time.deltaTime * Speed );
		transform.rotation = Quaternion.Euler (0, 0, 45);
	}
	
	protected void MoveUp ()
	{
		rigidbody2D.AddForce( Vector2.up * Time.deltaTime * Speed );
		transform.rotation = Quaternion.Euler (0, 0, 315);
	}
	
	protected void MoveDown ()
	{
		rigidbody2D.AddForce( -Vector2.up * Time.deltaTime * Speed );
		transform.rotation = Quaternion.Euler (0, 0, 135);
	}
	
	protected void LateUpdate() {
		Vector3 vel = rigidbody2D.velocity;
		vel *= Damping;
		rigidbody2D.velocity = vel;
	}
}
