﻿using UnityEngine;
using System.Collections;

public class FloorPlanWinScript : MonoBehaviour {

	public CUTSCENE NextCutscene;

	void OnTriggerEnter2D(Collider2D other) {
		if( other.gameObject.tag == "Enemy") {
			Win();
		}
	}

	void Win() {
		PlayerPrefs.SetInt( CutsceneManager.CutsceneSave, (int)NextCutscene );
		Application.LoadLevel("cutscene");
	}

}
