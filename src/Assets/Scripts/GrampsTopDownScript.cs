﻿using UnityEngine;
using System.Collections;

public class GrampsTopDownScript : FloorEntity {

	public int Direction;
	private int LastDirection = 0;

	void Start() {
		RandomDirection();
		base.Start();
	}

	// Update is called once per frame
	void Update () {
		switch( Direction ) {
		case 0:
			MoveUp();
			break;
		case 1:
			MoveRight();
			break;
		case 2:
			MoveDown();
			break;
		case 3:
			MoveLeft();
			break;
		}

		float MoveSpeed = rigidbody2D.velocity.magnitude;
		anim.SetFloat("speed", MoveSpeed);
	}

	void RandomDirection() {
		do {
			Direction = Random.Range (0,4);
		} while( Direction == LastDirection );

		LastDirection = Direction;
		Invoke("RandomDirection", Random.Range (1, 3));
	}

	void OnCollisionEnter2D(Collision2D other) {
		Debug.Log (other.gameObject.tag);
		if(other.gameObject.tag == "Wall") {
			Sound.Instance.PlaySFX(Sound.WALL);
		}
	}
}
