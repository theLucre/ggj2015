﻿using UnityEngine;
using System.Collections;

public class SpaceToContinueScript : MonoBehaviour {

	public float WaitTime;
	public string LevelName;
	bool AllowLoad = false;
	public CUTSCENE NextCutscene;
	public string SFX;
	bool HasClicked = false;
	public BGMScript music;

	// Use this for initialization
	void Start () {

	}

	void Update() {
		if( Time.timeSinceLevelLoad > WaitTime ) {
			AllowLoad = true;
		}
		if( Input.GetButtonDown("jump") && AllowLoad == true && !HasClicked ) {
			Sound.Instance.PlaySFX( SFX );
			Invoke("Load", 0.5f);
			HasClicked = true;
		}
	}

	void Load() {
		PlayerPrefs.SetInt( CutsceneManager.CutsceneSave, (int)NextCutscene );
		Application.LoadLevel( LevelName );
	}
}
