﻿using UnityEngine;
using System.Collections;

public class WaitScreenScript : MonoBehaviour {

	public float WaitTime;
	public string LevelName;

	// Use this for initialization
	void Start () {
		Sound.Instance.PlaySFX(Sound.LUCRE);
		Invoke ("Load", WaitTime );
	}
	
	void Load() {
		Application.LoadLevel( LevelName );
	}
}