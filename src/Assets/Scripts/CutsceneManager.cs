﻿using UnityEngine;
using System.Collections;

public enum CUTSCENE {
	INTRO,
	DESKTOP,
	BROWSER,
	SANDWICH,
	EMAIL,
	TYPING,
	ENDING,
	DESKTOP_AGAIN
}

public class CutsceneManager : Singleton<CutsceneManager> {

	public static string CutsceneSave = "cutscene";
	public CUTSCENE CurrentCutScene;
	public CutsceneScript SceneScript;

	// Use this for initialization
	void Start () {
		CurrentCutScene = (CUTSCENE)PlayerPrefs.GetInt( CutsceneManager.CutsceneSave );

		GameObject[] cutscenes = GameObject.FindGameObjectsWithTag("Cutscene");
		foreach(GameObject go in cutscenes) {
			CutsceneScript cs = go.GetComponent<CutsceneScript>();
			if( cs.Scene == CurrentCutScene ) 
				SceneScript = cs;
			cs.DisableChildren();
		}

		SceneScript.Next();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("jump"))
			SceneScript.Next();
	}
}
