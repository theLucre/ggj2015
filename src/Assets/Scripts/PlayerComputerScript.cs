﻿using UnityEngine;
using System.Collections;

public class PlayerComputerScript : MonoBehaviour {

	public float 
		Speed = 100,
		MaxSpeed = 13f,
		Damping = 0.99f;

	bool Stopped; 

	public bool PlayerController = false;
	public bool Pressing = false;
	public ScoreManagerScript ScoreManager;
	public Sprite HandNormal, HandPress;
	SpriteRenderer sr;
	// Use this for initialization
	void Start () {
		sr = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if( Stopped ) return;

		float xAxis = Input.GetAxis("Horizontal");
		float yAxis = Input.GetAxis("Vertical");
		Vector3 move = new Vector2( xAxis, yAxis ) * Time.deltaTime * Speed;
		if( move.magnitude > MaxSpeed ) {
			move.Normalize();
			move *= MaxSpeed;
		}

		rigidbody2D.AddForce( move );

		if( PlayerController && Input.GetButtonDown("jump") && !Pressing) {
			Pressing = true;
			sr.sprite = HandPress;
			Invoke ("UnPress", 0.4f);
		}
	}

	void UnPress() {
		Pressing = false;
		sr.sprite = HandNormal;
	}

	void LateUpdate() {
		Vector3 vel = rigidbody2D.velocity;
		vel *= Damping;
	}

	public void AddKey() {
		ScoreManager.IncrementKeys(  );
	}

	public void AddTotalKey(string character) {
		ScoreManager.AddTotal(character);
	}

	public void Stop() {
		Stopped = true;
		Destroy ( gameObject );
	}
}
