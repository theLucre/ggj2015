﻿using UnityEngine;
using System.Collections;

public class CutsceneScript : MonoBehaviour {

	public CUTSCENE Scene;
	public GameObject[] SceneImages;
	public string LevelToLoad;
	public int CurrentScript = -1;

	public void DisableChildren() {
		Transform[] ts = gameObject.GetComponentsInChildren<Transform>() ;
		foreach(Transform t in ts) {
			if( t != transform && t.parent == transform ) {
				t.gameObject.SetActive(false);
			}
		}
	}

	public void Next() {
		if( CurrentScript >= 0 )
			SceneImages[CurrentScript].SetActive(false);
		CurrentScript++;

		if( CurrentScript >= SceneImages.Length)
			Application.LoadLevel( LevelToLoad );
		else 
			SceneImages[CurrentScript].SetActive(true);
	}
}
