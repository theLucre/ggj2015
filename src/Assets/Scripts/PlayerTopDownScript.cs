﻿using UnityEngine;
using System.Collections;

public class PlayerTopDownScript : FloorEntity {

	void Start() {
		Damping = 0.9f;
		base.Start();
	}

	void Update() {

		float xAxis = Input.GetAxis("Horizontal");
		float yAxis = Input.GetAxis("Vertical");

		if( xAxis > 0 ) MoveRight();
		else if( xAxis < 0 ) MoveLeft();
		if( yAxis > 0 ) MoveUp();
		else if( yAxis < 0 ) MoveDown();


		float MoveSpeed = new Vector2( xAxis, yAxis).magnitude;
		anim.SetFloat("speed", MoveSpeed);
	}

}
