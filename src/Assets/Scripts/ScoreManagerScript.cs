﻿using UnityEngine;
using System.Collections;

public class ScoreManagerScript : MonoBehaviour {

	public int KeysNeeded = 40;
	public int KeysHave = 0;
	public int TotalKeys = 0;
	public string Email;
	TextMesh tm;
	public TextMesh EmailText;
	public PlayerComputerScript Player;
	bool Won = false;
	public FloorCameraScript cam;
	public Transform newTarget;
	public GameObject FinalCursor;

	// Use this for initialization
	void Start () {
		tm = gameObject.GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Won)
			tm.text = "";
		else
			tm.text = KeysHave + "/" + KeysNeeded;
	}

	public void IncrementKeys() {
		KeysHave++;
		if(KeysHave >= KeysNeeded) {
			Won = true;
			Player.enabled = false;
			FinalCursor.SetActive(true);
			Sound.Instance.PlaySFX(Sound.VICTORY);
			cam.Target = newTarget;
		}
	}

	public void AddTotal(string character) {
		TotalKeys++;
		EmailText.text += character;
		if( EmailText.text.Length % 35 == 0 ) EmailText.text += '\n';
	}
}
