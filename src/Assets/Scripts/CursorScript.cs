﻿using UnityEngine;
using System.Collections;

public class CursorScript : MonoBehaviour {

	public enum TYPE {
		RANDOM,
		TARGET
	}

	public BoxCollider2D RandomArea;
	public float Speed, SpeedVariance;
	public TYPE Type;
	public Transform TargetObject;
	public ComputerManagerScript Manager;
	float CurrentSpeed;
	Vector3 TargetPoint = Vector2.zero;
	bool Stopped = false;
	public Transform TrashTarget;
	public Transform BrowserIcon;
	public bool ShouldRightClick;
	public GameObject RightClickMenu;

	// Use this for initialization
	void Start () {
		SetTargetPoint();
		if( ShouldRightClick ) 
			Invoke ("RightClick", Random.Range (2, 6));
	}

	void RightClick() {
		RightClickMenu.SetActive(true);
		Vector3 pos = transform.position;
		pos.z = RightClickMenu.transform.position.z;
		RightClickMenu.transform.position = pos;
		Sound.Instance.PlaySFX(Sound.CLICK);
		Invoke ("RightClick", Random.Range (2, 6));
	}

	// Update is called once per frame
	void Update () {
		if( !Stopped )
			MoveTowardsTarget();
	}

	void SetTargetPoint() {
		if( Type == TYPE.RANDOM ) {
			TargetPoint = new Vector3(
				Random.Range ( RandomArea.center.x - RandomArea.bounds.extents.x, RandomArea.center.x + RandomArea.bounds.extents.x ),
				Random.Range ( RandomArea.center.y - RandomArea.bounds.extents.y, RandomArea.center.y + RandomArea.bounds.extents.y ), 0
			);
			Invoke ("SetTargetPoint", 7);
		} else if( Type == TYPE.TARGET ) {
			TargetPoint = new Vector3( TargetObject.position.x, TargetObject.position.y, 0 );
		}
		CurrentSpeed = Speed + Random.Range ( -SpeedVariance, SpeedVariance );

	}

	void MoveTowardsTarget() {
		Vector3 dir = TargetPoint - transform.position;
		dir.Normalize();
		rigidbody2D.AddForce( dir * Time.deltaTime * CurrentSpeed );
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.name == "TargetIcon") {
			Manager.Win();
		} else if(other.gameObject.name == "LoseIcon") {
			Manager.Lose();
		} 
		if(other.gameObject.name == "FailIcon") {
			rigidbody2D.velocity = Vector2.zero;
			Invoke("DragIcon", 1);
		}
		if( other.gameObject.name == "TrashIcon") {
			Stop ();
			Destroy( BrowserIcon.gameObject );
			Invoke ("Finale", 2f);
			Sound.Instance.PlaySFX(Sound.TRASH);
		}
	}

	void Finale() {
		// BEAT THE BROWSER LEVEL
		Application.LoadLevel("fuck");
	}

	void DragIcon() {
		TargetPoint = TrashTarget.position;
		BrowserIcon.parent = transform;
		Sound.Instance.PlaySFX(Sound.CLICK);
	}

	public void Stop() {
		Stopped = true;
		rigidbody2D.isKinematic = true;
		rigidbody2D.Sleep ();
		collider2D.enabled = false;
		CancelInvoke("SetTargetPoint");
		CancelInvoke("RightClick");
	}
}
