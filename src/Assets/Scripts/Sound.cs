﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum AUDIO_TYPE {
	SFX,
	BGM
};

public class Sound : Singleton<Sound> {

	/****************************************************
	 * Use as Game.Instance.Sound.Play( Sound.KEY )
	 ****************************************************/

	//Control
	public bool muteGlobal;
	private Dictionary<string, AudioClip> SFX;

	private static string AUDIO_RESOURCES_DIR = "Audio";
	private static string SFX_DIR = "SFX";
	private static string BGM_DIR = "BGM";

	/****************************************************
	 * SFX KEYS
	 ****************************************************/
	public static string 
		CLICK = "click",
		CLOSE = "close",
		GRANDMA = "grandma",
		GRANDPA = "grandpa",
		OH = "oh",
		JOHNNY = "johnny",
		START = "start",
		WALL = "wall",
		VASE = "vase",
		FUCK = "fuck",
		PICTURE = "picture",
		POT = "pot",
		TRASH = "trash",
		LUCRE = "lucre",
		KEY = "keyboard",
		VICTORY = "victory"
		;


	// BGM KEYS
	public static string 
		BGM = "bgm",
		TITLE = "title",
		CREDITS = "credits";

	private AudioSource audioSource;

	public Sound ()
	{
		SFX = new Dictionary<string, AudioClip>();

		// sounds
		AddSFXClip ( CLICK );
		AddSFXClip ( CLOSE );
		AddSFXClip ( GRANDMA );
		AddSFXClip ( GRANDPA );
		AddSFXClip ( JOHNNY );
		AddSFXClip ( OH );
		AddSFXClip ( START );
		AddSFXClip ( WALL );
		AddSFXClip ( VASE );
		AddSFXClip ( FUCK );
		AddSFXClip ( PICTURE);
		AddSFXClip ( POT );
		AddSFXClip ( TRASH );
		AddSFXClip ( LUCRE );
		AddSFXClip ( KEY );
		AddSFXClip ( VICTORY );

		// music
		AddSFXClip( BGM, AUDIO_TYPE.BGM );
		AddSFXClip( TITLE, AUDIO_TYPE.BGM );
		AddSFXClip( CREDITS, AUDIO_TYPE.BGM );

	}

	private void AddSFXClip ( string file, AUDIO_TYPE type = AUDIO_TYPE.SFX ) 
	{	
		string dir = ( type == AUDIO_TYPE.SFX ) ? SFX_DIR : BGM_DIR;
			SFX.Add ( file , (AudioClip) Resources.Load ( AUDIO_RESOURCES_DIR + "/" + dir + "/" + file, typeof(AudioClip)) );
	}

	void Awake()
	{
		audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.panLevel = 0.0f;
	}

	// METHODS
	public void PlaySFX(string clipToPlay)
	{
		AudioClip tmp;
		if( SFX.TryGetValue( clipToPlay, out tmp ) )
		{
			try 
			{
				audioSource.PlayOneShot ( tmp );
			} catch ( System.Exception e ) {
				Debug.Log ( clipToPlay + " failed to play" );
			}
		}
		else 
			Debug.Log ( "sound SFX could not be be played : " + clipToPlay ); 
	}

	public bool PlayBGM( string musicToPlay )
	{
		AudioClip tmp;
		if( SFX.TryGetValue( musicToPlay, out tmp ) )
		{
			if( tmp == null )
				return false;
			audioSource.clip = tmp;
			audioSource.Play ();
			return true;
		}
		else 
			Debug.Log ( "sound SFX could not be be played : " + musicToPlay );  
		return false;
	}

	public void StopMusic()
	{
		audioSource.Stop();
	}


}








