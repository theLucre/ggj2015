﻿using UnityEngine;
using System.Collections;

public class ComputerManagerScript : MonoBehaviour {

	public PlayerComputerScript Player;
	public CursorScript Cursor;
	public Transform Browser;
	bool HasInit = false;
	bool HasWon = false, HasLost = false;
	public bool CloseBrowser = false;
	public bool TypingScene = false;

	public void Init() {
	}
	
	// Update is called once per frame
	void Update () {
		if(HasWon) {
			if( TypingScene ) {
				Sound.Instance.PlaySFX(Sound.CLICK);
				Cursor.Stop();
				PlayerPrefs.SetInt( CutsceneManager.CutsceneSave, (int)CUTSCENE.ENDING );
				Application.LoadLevel("cutscene");
				Debug.Log ("LoadingLevel");
				return;
			}

			if(CloseBrowser) {
				// BEAT THE BROWSER LEVEL
				PlayerPrefs.SetInt( CutsceneManager.CutsceneSave, (int)CUTSCENE.TYPING );
				Application.LoadLevel("cutscene");
			} else { 
				Browser.localScale *= 1.04f;
				if(Browser.localScale.x > 1 ) {
					// BEAT THE DESKTOP LEVEL
					Browser.localScale = Vector3.one;
					PlayerPrefs.SetInt( CutsceneManager.CutsceneSave, (int)CUTSCENE.BROWSER );
					Application.LoadLevel("cutscene");
				}
			}
		}

		if(HasLost) {
			if(CloseBrowser) {
				Browser.localScale *= 0.96f;
				if(Browser.localScale.x < 0.01f ) {
					// FAILED BROWSER LEVEL
					Browser.localScale = Vector3.zero;
					PlayerPrefs.SetInt( CutsceneManager.CutsceneSave, (int)CUTSCENE.DESKTOP_AGAIN );
					Application.LoadLevel("cutscene");
				}
			}
		}
	}

	public void Win() {
		HasWon = true;
		Player.Stop();
		Cursor.Stop();
		Sound.Instance.PlaySFX(Sound.CLICK);
		if(!CloseBrowser) 
			Browser.localScale = new Vector3(0.01f, 0.01f, 0);

	}

	public void Lose() {
		Sound.Instance.PlaySFX(Sound.CLICK);
		HasLost = true;
		Player.Stop();
		Cursor.Stop();
	}
}
